from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer
from tkinter import *
import pyttsx3 as pp


engine=pp.init()

voices=engine.getProperty('voices')
# print(voices)
engine.setProperty('voice',voices[1].id)  #0 for male 1 for female 

def speak(word):
    engine.say(word)
    engine.runAndWait()

bot=ChatBot("My Bot")
convo =["Hello",
    "Hi there!",
    "How are you doing?",
    "I'm doing great.",
    "That is good to hear",
    "Thank you.",
    " I live in Mumbai, Thanks to my Boss Mr. Girish Tanna!",
    "My name is bot . I am created by Mr. Girish Tanna",
    "You're welcome."]
trainer=ListTrainer(bot)


trainer.train(convo)   #Training the bot with the help of traier



# answer=bot.get_response("What is your Name?")
# print(answer)
# print('Talk to bot')
# while True:
#     ques=input("Ask the bot?")
#     if ques == 'exit':
#         break
#     else:
#         ans=bot.get_response(ques)
#         print('bot: ',ans)

main= Tk()



main.geometry("500x650") #geometry methods sets the height and width of the tkinter window
main.title("My Chat bot")
img=PhotoImage(file="bot1.png")
photoL=Label(main,image=img)

photoL.pack(pady=5)

def ask_from_bot():
    query=textF.get()
    answer_from_bot=bot.get_response(query)
    msgs.insert(END,"you : "+query)
    
    msgs.insert(END,"bot : "+ str(answer_from_bot))
    speak(answer_from_bot)
    textF.delete(0,END)
    msgs.yview(END)


frame=Frame(main)
sc=Scrollbar(frame)

msgs=Listbox(frame,width=80,height=20,yscrollcommand=sc.set)

sc.pack(side=RIGHT,fill=Y)
msgs.pack(side=LEFT,fill=BOTH,pady=10)
frame.pack()


# Creating Text feild

textF=Entry(main,font=("Verdana",20))
textF.pack(fill=X,pady=10)


# ask btn
btn=Button(main,text="Ask From bot",font=("verdana",20),command=ask_from_bot)
btn.pack()


# creating a function

def enter_function(event):
    btn.invoke()
# going o bind mainwindow with enter key

main.bind('<Return>',enter_function)   #keyCode for Enter is Return
main.mainloop()